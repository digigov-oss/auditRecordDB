#!/bin/bash
#this tool stress FileEngine and System  

# to avoid mesh
killall node > /dev/null 2>&1
yarn build > /dev/null 2>&1

echo "Stress test Starting, if you want to stop it, press Ctrl+C"

# clean up
rm -rf /tmp/audit > /dev/null 2>&1
mkdir -p /tmp/audit > /dev/null 2>&1

# Run 1000 times paralel the generateAuditRecord with FileEngine
for i in {1..1000}; do
    node --no-addons --no-deprecation --trace-warnings --trace-exit --experimental-specifier-resolution=node ./stressnode.js&
    sleep 0.01 #small nap to avoid 100% cpu usage
done

sleep 1

# validate by count the number of records on folder /tmp/audit is equal to 1000
while [ $(ls -1 /tmp/audit/*.json | wc -l) -ne 1000 ]; do
    echo -ne $`ls -1 /tmp/audit/*.json | wc -l`;
    sleep 1
done
