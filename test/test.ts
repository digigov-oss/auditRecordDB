import generateAuditRecord from '../src/index'
import {FileEngine} from '../src/engines/index';

const test = async () => {
    const ar = await generateAuditRecord();
    console.log(ar);
}

const test2 = async () => {
    const ar = await generateAuditRecord(undefined, new FileEngine('/tmp'));
    console.log(ar);
}

const test3 = async () => {
    const ar = await generateAuditRecord(undefined, new FileEngine('/tmp', 'daily'));
    console.log(ar);
}

const test4 = async () => {
    const ar = await generateAuditRecord(undefined, new FileEngine('/tmp', 'monthly'));
    console.log(ar);
}

const test5 = async () => {
    const ar = await generateAuditRecord(undefined, new FileEngine(undefined, 'yearly'));
    console.log(ar);
}

console.log("Audit Record Generator, simple test");
test();

console.log("Audit Record Generator, change file engine path");
test2();

console.log("Audit Record Generator, use daily sequence");
test3();

console.log("Audit Record Generator, use monthly sequence");
test4();

console.log("Audit Record Generator, use yearly sequence");
test5();