/**
 * @description Record to be stored in the database
 * @note Must be fullfiled completely, even if it is empty
 */
export declare type AuditRecord = {
    auditUnit?: string;
    auditTransactionId?: string;
    auditProtocol?: string;
    auditTransactionDate?: string;
    auditUserIp?: string;
    auditUserId?: string;
};
/**
 * @description AuditEngine interface
 * @note This interface is used to define the methods that must be implemented by the AuditEngine look at FileEngine.ts for an example
 * @interface AuditEngine
 */
export interface AuditEngine {
    put: (record: AuditRecord) => AuditRecord | Promise<AuditRecord>;
    get: (auditTransactionId: string) => AuditRecord | Promise<AuditRecord>;
    seq: (path?: string) => number | Promise<number>;
    pn: (reset?: "daily" | "monthly" | "yearly" | "innumerable", path?: string) => string | Promise<string>;
}
/**
 * @description FileSystem errors
 */
export declare type FS_ERROR = {
    code: string;
    message: string;
};
export declare type PNRESETTYPES = "daily" | "monthly" | "yearly" | "innumerable";
/**
 * @description Schema to be used to store the audit records on real databases
 */
export declare type DatabaseSettings = {
    tableName?: string;
    columns?: AuditRecord;
};
