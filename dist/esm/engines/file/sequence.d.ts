/**
 * update the sequence number on giveb sequence file
 * @param seqfile
 * @param lockfile
 * @returns number
 */
declare const sequence: (seqfile?: string, lockfile?: string) => number;
export default sequence;
