import { PNRESETTYPES } from '../../interfaces/index.js';
/**
 * Generate a new protocol number
 * in the format sequence/date
 * each day a new sequence is generated
 * @param protocol_path
 * @returns string
 */
declare const protocol: (protocol_path: string, pnreset: PNRESETTYPES) => string;
export default protocol;
