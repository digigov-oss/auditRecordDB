import { AuditRecord, AuditEngine } from './interfaces/index.js';
/**
 * @description Use this on your app to generate and log the audit record
 * @coment This is the main function of the application
 * @comment The audit record is stored in the database,
 * @comment you can provide a custom audit record, or use the generated one
 * @param auditInit - The audit record to be stored, if is empty a new one will be generated
 * @param storagePath - The path where the audit record will be stored, default is the tmp directory SOULD change this for production
 * @env env.HOSTIP - useful to pass the IP address of the end user automatically on docker enviroments
 * @returns AuditRecord | null - The audit record generated or the one provided
 */
export declare const generateAuditRecord: (auditInit?: AuditRecord | undefined, dbEngine?: AuditEngine | undefined) => Promise<AuditRecord | null>;
export default generateAuditRecord;
export { FileEngine } from './engines/index.js';
export { AuditRecord, AuditEngine, DatabaseSettings, FS_ERROR, PNRESETTYPES } from './interfaces/index.js';
