"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FileEngine = exports.generateAuditRecord = void 0;
const db_js_1 = __importDefault(require("./lib/db.js"));
const index_js_1 = require("./engines/index.js");
/**
 * @description Use this on your app to generate and log the audit record
 * @coment This is the main function of the application
 * @comment The audit record is stored in the database,
 * @comment you can provide a custom audit record, or use the generated one
 * @param auditInit - The audit record to be stored, if is empty a new one will be generated
 * @param storagePath - The path where the audit record will be stored, default is the tmp directory SOULD change this for production
 * @env env.HOSTIP - useful to pass the IP address of the end user automatically on docker enviroments
 * @returns AuditRecord | null - The audit record generated or the one provided
 */
const generateAuditRecord = (auditInit, dbEngine) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const dbe = dbEngine || new index_js_1.FileEngine("/tmp");
        const _db = new db_js_1.default(dbe);
        const auditUnit = (auditInit === null || auditInit === void 0 ? void 0 : auditInit.auditUnit) || "gov.gr";
        const auditTransactionId = (auditInit === null || auditInit === void 0 ? void 0 : auditInit.auditTransactionId) || "" + (yield _db.seq());
        const auditProtocol = (auditInit === null || auditInit === void 0 ? void 0 : auditInit.auditProtocol) || (yield _db.pn());
        const auditTransactionDate = (auditInit === null || auditInit === void 0 ? void 0 : auditInit.auditTransactionDate) || new Date().toISOString().split('.')[0] + "Z";
        const auditUserIp = (auditInit === null || auditInit === void 0 ? void 0 : auditInit.auditUserIp) || process.env.HOSTIP || "127.0.0.1";
        const auditUserId = (auditInit === null || auditInit === void 0 ? void 0 : auditInit.auditUserId) || "system";
        const auditRecord = {
            auditUnit,
            auditTransactionId,
            auditProtocol,
            auditTransactionDate,
            auditUserIp,
            auditUserId
        };
        return yield _db.put(auditRecord);
    }
    catch (error) {
        const err = error;
        console.log(err.code, err.message);
        return null;
    }
});
exports.generateAuditRecord = generateAuditRecord;
exports.default = exports.generateAuditRecord;
var index_js_2 = require("./engines/index.js");
Object.defineProperty(exports, "FileEngine", { enumerable: true, get: function () { return index_js_2.FileEngine; } });
