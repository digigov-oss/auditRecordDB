"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sequence_js_1 = __importDefault(require("./sequence.js"));
/**
 * Generate a new protocol number
 * in the format sequence/date
 * each day a new sequence is generated
 * @param protocol_path
 * @returns string
 */
const protocol = (protocol_path, pnreset) => {
    let protocol_split = "aion";
    let protocol_date = new Date().toISOString().split('T')[0];
    switch (pnreset) {
        case "daily":
            protocol_split = protocol_date;
            break;
        case "monthly":
            protocol_split = protocol_date.split('-')[0] + "-" + protocol_date.split('-')[1];
            break;
        case "yearly":
            protocol_split = protocol_date.split('-')[0];
            break;
        case "innumerable":
            protocol_split = "aion";
            break;
    }
    const path = `${protocol_path}/${protocol_split}.protocol.sequence`;
    const protocol_sequence = (0, sequence_js_1.default)(path, path); //Protocol starts from 1 depent on pnreset.
    let pn = protocol_sequence + "/" + protocol_date;
    return pn;
};
exports.default = protocol;
