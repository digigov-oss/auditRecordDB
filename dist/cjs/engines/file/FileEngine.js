"use strict";
var __classPrivateFieldSet = (this && this.__classPrivateFieldSet) || function (receiver, state, value, kind, f) {
    if (kind === "m") throw new TypeError("Private method is not writable");
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a setter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot write private member to an object whose class did not declare it");
    return (kind === "a" ? f.call(receiver, value) : f ? f.value = value : state.set(receiver, value)), value;
};
var __classPrivateFieldGet = (this && this.__classPrivateFieldGet) || function (receiver, state, kind, f) {
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a getter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot read private member from an object whose class did not declare it");
    return kind === "m" ? f : kind === "a" ? f.call(receiver) : f ? f.value : state.get(receiver);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _FileEngine_path, _FileEngine_pnreset;
Object.defineProperty(exports, "__esModule", { value: true });
exports.FileEngine = void 0;
//Use File System as DB storage
const fs_1 = __importDefault(require("fs"));
const protocol_js_1 = __importDefault(require("./protocol.js"));
const sequence_js_1 = __importDefault(require("./sequence.js"));
/**
 * @description AuditEngine implementation
 * @note This class is used to implement the methods that must be implemented by the AuditEngine
 * @class FileEngine
 * @implements AuditEngine
 * @param {string} path - path to store the records
 * @param {PNRESETTYPES} pnreset - protocol number sequence type
 */
class FileEngine {
    constructor(path, pnreset) {
        _FileEngine_path.set(this, void 0);
        _FileEngine_pnreset.set(this, void 0);
        __classPrivateFieldSet(this, _FileEngine_path, path ? path : "/tmp", "f");
        __classPrivateFieldSet(this, _FileEngine_pnreset, pnreset || "innumerable", "f");
    }
    /**
     * @description Store a record in the database
     * @param {AuditRecord} record - record to be stored
     * @returns {AuditRecord} - the record stored
     * @memberof FileEngine
     * @method put
     */
    put(record) {
        const data = JSON.stringify(record, null, 2);
        try {
            fs_1.default.writeFileSync(__classPrivateFieldGet(this, _FileEngine_path, "f") + '/record-' + record.auditTransactionId + '.json', data);
            return record;
        }
        catch (error) {
            throw error;
        }
    }
    /**
     * @description Get a record from the database
     * @param auditTransactionId
     * @returns {AuditRecord}
     * @memberof FileEngine
     * @method get
     */
    get(auditTransactionId) {
        try {
            const data = fs_1.default.readFileSync(__classPrivateFieldGet(this, _FileEngine_path, "f") + '/record-' + auditTransactionId + '.json', 'utf8');
            return JSON.parse(data);
        }
        catch (error) {
            throw error;
        }
    }
    /**
     * @description Generate a new sequence number
     * @param path
     * @returns number
     * @memberof FileEngine
     * @method seq
     */
    seq(path) {
        const sequence_save_path = path || __classPrivateFieldGet(this, _FileEngine_path, "f");
        try {
            return (0, sequence_js_1.default)(sequence_save_path + "/sequence", sequence_save_path + "/sequence");
        }
        catch (error) {
            throw error;
        }
    }
    /**
     * @description Generate a new protocol number
     * @param type: SEQTYPES
     * @param path: string
     * @returns string
     * @memberof FileEngine
     * @method protocol
     */
    pn(reset, path) {
        const protocol_save_path = path || __classPrivateFieldGet(this, _FileEngine_path, "f");
        const protocol_reset = reset || __classPrivateFieldGet(this, _FileEngine_pnreset, "f");
        try {
            return (0, protocol_js_1.default)(protocol_save_path, protocol_reset);
        }
        catch (error) {
            throw error;
        }
    }
}
exports.FileEngine = FileEngine;
_FileEngine_path = new WeakMap(), _FileEngine_pnreset = new WeakMap();
exports.default = FileEngine;
