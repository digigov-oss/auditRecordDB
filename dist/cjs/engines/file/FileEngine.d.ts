import { AuditRecord, AuditEngine, PNRESETTYPES } from '../../interfaces/index.js';
/**
 * @description AuditEngine implementation
 * @note This class is used to implement the methods that must be implemented by the AuditEngine
 * @class FileEngine
 * @implements AuditEngine
 * @param {string} path - path to store the records
 * @param {PNRESETTYPES} pnreset - protocol number sequence type
 */
export declare class FileEngine implements AuditEngine {
    #private;
    constructor(path?: string, pnreset?: PNRESETTYPES);
    /**
     * @description Store a record in the database
     * @param {AuditRecord} record - record to be stored
     * @returns {AuditRecord} - the record stored
     * @memberof FileEngine
     * @method put
     */
    put(record: AuditRecord): AuditRecord;
    /**
     * @description Get a record from the database
     * @param auditTransactionId
     * @returns {AuditRecord}
     * @memberof FileEngine
     * @method get
     */
    get(auditTransactionId: string): AuditRecord;
    /**
     * @description Generate a new sequence number
     * @param path
     * @returns number
     * @memberof FileEngine
     * @method seq
     */
    seq(path?: string): number;
    /**
     * @description Generate a new protocol number
     * @param type: SEQTYPES
     * @param path: string
     * @returns string
     * @memberof FileEngine
     * @method protocol
     */
    pn(reset?: PNRESETTYPES, path?: string): string;
}
export default FileEngine;
