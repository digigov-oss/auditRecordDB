"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FileEngine = void 0;
var FileEngine_js_1 = require("./file/FileEngine.js"); // Language: typescript, note the extension, sould be .js! even if it is .ts for esm to work correctly
Object.defineProperty(exports, "FileEngine", { enumerable: true, get: function () { return __importDefault(FileEngine_js_1).default; } });
