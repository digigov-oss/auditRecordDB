"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.db = void 0;
/**
 * @description AuditEngine implementation
 * @note This class uses AuditEngine implemetations, look at FileEngine.ts for an example
 * @class db
 * @param {AuditEngine} engine - AuditEngine implementation
 */
class db {
    constructor(engine) {
        this.engine = engine;
    }
    /**
     * @description Store a record in the database
     * @param record:AuditRecord
     * @returns Promise<AuditRecord>
     * @memberof db
     * @method put
     */
    put(record) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!record.auditTransactionId)
                throw new Error("record.auditTransactionId is required");
            return yield this.engine.put(record);
        });
    }
    /**
     * @description Get a record from the database
     * @param auditTransactionId:string
     * @returns Promise<AuditRecord>
     * @memberof db
     * @method get
     */
    get(auditTransactionId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!auditTransactionId)
                throw new Error("auditTransactionId is required");
            return yield this.engine.get(auditTransactionId);
        });
    }
    /**
     * @description Generate a new sequence number
     * @param path:string
     * @returns Promise<number>
     * @memberof db
     * @method seq
     */
    seq(path) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.engine.seq(path);
        });
    }
    /**
     * @description Generate a new protocol string
     * @param type:PNRESETTYPES
     * @param path:string
     * @returns string
     * @memberof db
     * @method pn
     */
    pn(reset, path) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.engine.pn(reset, path);
        });
    }
}
exports.db = db;
exports.default = db;
