import { AuditRecord, AuditEngine, PNRESETTYPES } from '../interfaces/index.js';
/**
 * @description AuditEngine implementation
 * @note This class uses AuditEngine implemetations, look at FileEngine.ts for an example
 * @class db
 * @param {AuditEngine} engine - AuditEngine implementation
 */
export declare class db {
    engine: AuditEngine;
    constructor(engine: AuditEngine);
    /**
     * @description Store a record in the database
     * @param record:AuditRecord
     * @returns Promise<AuditRecord>
     * @memberof db
     * @method put
     */
    put(record: AuditRecord): Promise<AuditRecord>;
    /**
     * @description Get a record from the database
     * @param auditTransactionId:string
     * @returns Promise<AuditRecord>
     * @memberof db
     * @method get
     */
    get(auditTransactionId: string): Promise<AuditRecord>;
    /**
     * @description Generate a new sequence number
     * @param path:string
     * @returns Promise<number>
     * @memberof db
     * @method seq
     */
    seq(path?: string): Promise<number>;
    /**
     * @description Generate a new protocol string
     * @param type:PNRESETTYPES
     * @param path:string
     * @returns string
     * @memberof db
     * @method pn
     */
    pn(reset?: PNRESETTYPES, path?: string): Promise<string>;
}
export default db;
