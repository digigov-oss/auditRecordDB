/**
 * @description Record to be stored in the database
 * @note Must be fullfiled completely, even if it is empty
 */
export type AuditRecord = {
    auditUnit?: string, // name of the institution calling the internet service
    auditTransactionId?: string, // a/a call of the application
    auditProtocol?: string, // protocol number
    auditTransactionDate?: string, // the date & time of the call as iso string
    auditUserIp?: string,  // the IP address of the end user
    auditUserId?: string, // end user ID
};

/**
 * @description AuditEngine interface
 * @note This interface is used to define the methods that must be implemented by the AuditEngine look at FileEngine.ts for an example
 * @interface AuditEngine
 */
export interface AuditEngine {
    put: (record: AuditRecord) => AuditRecord | Promise<AuditRecord>;
    get: (auditTransactionId: string) => AuditRecord | Promise<AuditRecord>;
    seq: (path?: string) => number | Promise<number>; //Generate a new sequence number
    pn: (reset?:"daily"|"monthly"|"yearly"|"innumerable",path?: string) => string | Promise<string>; //Generate a new protocol string
}

/**
 * @description FileSystem errors
 */
export type FS_ERROR = {
    code: string,
    message: string
}

export type PNRESETTYPES = "daily"|"monthly"|"yearly"|"innumerable";

/**
 * @description Schema to be used to store the audit records on real databases
 */
export type DatabaseSettings = {
    tableName?:string,
    columns?:AuditRecord
}