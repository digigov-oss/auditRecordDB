import { AuditRecord, AuditEngine, PNRESETTYPES } from '../interfaces/index.js';

/**
 * @description AuditEngine implementation
 * @note This class uses AuditEngine implemetations, look at FileEngine.ts for an example
 * @class db
 * @param {AuditEngine} engine - AuditEngine implementation
 */
export class db {
    engine: AuditEngine;
    constructor(engine: AuditEngine) {
        this.engine = engine;
    }

    /**
     * @description Store a record in the database
     * @param record:AuditRecord
     * @returns Promise<AuditRecord>
     * @memberof db
     * @method put
     */
    async put(record: AuditRecord): Promise<AuditRecord> {
        if (!record.auditTransactionId) throw new Error("record.auditTransactionId is required");
        return await this.engine.put(record);
    }

    /**
     * @description Get a record from the database
     * @param auditTransactionId:string 
     * @returns Promise<AuditRecord>
     * @memberof db
     * @method get
     */
    async get(auditTransactionId: string): Promise<AuditRecord> {
        if (!auditTransactionId) throw new Error("auditTransactionId is required");
        return await this.engine.get(auditTransactionId);
    }

    /**
     * @description Generate a new sequence number
     * @param path:string
     * @returns Promise<number>
     * @memberof db
     * @method seq
     */
    async seq(path?: string): Promise<number> {
        return await this.engine.seq(path);
    }

    /**
     * @description Generate a new protocol string
     * @param type:PNRESETTYPES
     * @param path:string
     * @returns string 
     * @memberof db
     * @method pn
     */
    async pn(reset?: PNRESETTYPES, path?: string): Promise<string> {
        return await this.engine.pn(reset, path);
    }

}

export default db;
