# AuditRecordDB
According to Common Guide for Client Applications of GSIS, (https://www.gsis.gr/dimosia-dioikisi/ked/koinos-odigos)
all online services are required to have the addition of tracking data (auditRecord) when calling them. 

This module implements a JSON file storage database for use with the audit mechanism of GSIS. It follows the interoperability_specsv1.4 and simplifies the integration with Nextjs/Nodejs projects. 

It also provides a way for automating protocol numbers and transactions Id's, if your app does not provide them.

# Usage
```
import auditRecordDB from 'AuditRecordDB';

const main = () =>{
   //returns generated AuditRecord, that record stored by default in `/tmp` folder using `FileEngine` SOULD change this for production
   console.log(auditRecordDB({auditUnit:'DigiGov'}))
   /*
   {
    auditUnit: 'DigiGov',
    auditTransactionId: '1',
    auditProtocol: '1/2022-01-01',
    auditTransactionDate: '2022-01-01T00:00:01Z',
    auditUserIp: '127.0.0.1',
    auditUserId: 'system'
   }
   */
}

```

```
//you can change the FileEngine storage path
import auditRecordDB,{FileEngine} from 'AuditRecordDB';
const main = () =>{
console.log(auditRecordDB({},new FileEngine('/tmp/auditRecords')))
}
```

```
//you can change the protocol reset sequence type
import auditRecordDB,{FileEngine} from 'AuditRecordDB';
const main = () =>{
console.log(auditRecordDB({},new FileEngine(undefined, 'daily')))
}
```

# AuditEngine
By default, the file storage engine `FileEngine` is used. 
Please keep in mind to change the storage path to app needs. 
`FileEngine` is a simple file storage engine, that stores the audit records in a file, also it is possible to use other storage engines.

Your app can also use `PostgreSqlEngine` provided from `@digigov-oss/auditrecord-postgresql-engine` module.

If you do not provide protocol numbers, the module will generate them for you. You can pass at the engine the type of protocol number reset you want to use from one of the following:
"daily", "monthly", "yearly". By default, innumerable protocol numbers are generated.


Look at `FileEngine.ts` here and `PostgreSqlEngine.ts` of the above module for examples to extend the store to another 'real' database.
